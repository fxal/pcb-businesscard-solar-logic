# Printed circuit board business card with solar cell powering logic gates
This is a landing page repository for people who received my solar logic pcb business card (or stumbled over this link somehow, hi there!).

Here you can find the bill of materials, schematics and board designs of the pcb business card.

Once assembled, this project lights up some LEDs depending on how the four DIP switches drive the logic gates in their respective configuration.

Also a single always-on LED on the front of the business card illuminates the [tittle](https://en.wikipedia.org/wiki/Tittle) of my name's "i".

There are four logic gates in different configurations featuring AND, NAND, OR and XOR output to their selected LEDs. Eacg gate has two inputs and one output.

Two different logic gates are in use for this project: One NC7SZ57 as an AND gate, three NC7SZ58 as NAND, OR and XOR gates.

A solar cell on the back side of the PCB powers the logic gates and the LEDs.

![](images/front.jpg)

![](images/back.jpg)

## Bill of materials
To complete this pcb business card, you will require the following hardware components (if you were given an unpopulated board):
- Logic Gate NC7SZ57 (Package: SOT-323 SMD) x1
- Logic Gate NC7SZ58 (Package: SOT-323 SMD) x3
- Solar Cell KXOB25-05X3F-TR (Package: 23mm x 8mm SMD) x1
- Resistor 330Ohm (Package: 0603 SMD) x5
- DIP Switch 2 Positions (Package: SMD) x4

## Images

![](images/businesscard_top.png)

![](images/businesscard_bottom.png)
